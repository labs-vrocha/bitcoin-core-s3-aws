FROM ruimarinho/bitcoin-core:24

RUN apt-get update \
    && apt-get install -y s3fs

ENTRYPOINT ["s3fs","$BUCKETNAME","$BUCKETPATH","-o","passwd_file=$BUCKETPASSFILE"]